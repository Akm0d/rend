"""
    tests.unit.rend.test_toml
    ~~~~~~~~~~~~~~

    Unit tests for the toml renderer
"""
import pytest

import rend.exc


def test_toml(mock_hub, hub):
    """
    test rend.toml.render renders correctly
    """
    data = """
    # I promise this is toml data
    title = 'toml test'
    [owner]
    name = 'toml owner'
    """
    mock_hub.rend.toml.render = hub.rend.toml.render
    ret = mock_hub.rend.toml.render(data)
    assert ret["title"] == "toml test"
    assert ret["owner"]["name"] == "toml owner"


def test_toml_bytes(mock_hub, hub):
    """
    test rend.toml.render renders correctly with bytes data
    """
    data = b"""
    # I promise this is toml data
    title = 'toml test'
    [owner]
    name = 'toml owner'
    """
    mock_hub.rend.toml.render = hub.rend.toml.render
    ret = mock_hub.rend.toml.render(data)
    assert ret["title"] == "toml test"
    assert ret["owner"]["name"] == "toml owner"


def test_toml_decode_error(mock_hub, hub):
    """
    test rend.toml.render when there is a decode error
    """
    data = """
    # I promise this is toml data
    title = 'toml test'
    [owner
    name = 'toml owner'
    """
    mock_hub.rend.toml.render = hub.rend.toml.render
    with pytest.raises(rend.exc.RenderException) as exc:
        mock_hub.rend.toml.render(data)
    assert exc.value.args[0] == "Toml render error: Key group not on a line by itself."
